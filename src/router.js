import Vue from "vue";
import Router from "vue-router";

const home = () => import("./views/Index.vue");
const teacher_index = () => import("./views/Teacher/VIP.vue");
const teacher_detail = () => import("./views/Teacher/video.vue");
const mine_index = () => import("./views/Mine/index.vue");
const active = () => import("./views/Active.vue");
const article = () => import("./views/Article.vue");

Vue.use(Router);
export default new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "首页",
      component: home
    },
    {
      path: "/teacher/index",
      name: "课程简介",
      component: teacher_index
    },
    {
      path: "/teacher/detail",
      name: "课程详情",
      component: teacher_detail
    },
    {
      path: "/mine/index",
      name: "个人中心",
      component: mine_index
    },
    {
      path: "/active",
      name: "课程之外",
      component: active
    },
    {
      path: "/article",
      name: "文章",
      component: article
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  }
});
