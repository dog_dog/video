// babel.config.js
module.exports = {
  presets: [
    [
      "@vue/app",
      {
        polyfills: ["es6.promise", "es6.symbol"],
        useBuiltIns: "entry",
        entry: {
          app: ["babel-polyfill", "./src/main.js"]
        }
      }
    ]
  ]
};
