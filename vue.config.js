module.exports = {
  publicPath: "./",
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "less",
      patterns: ["./src/assets/config.less"]
    }
  }
};
